import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {createStore, applyMiddleware, combineReducers,compose} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import dishFormReducer from "./store/reducers/dishFormReducer";
import getDishesReducer from "./store/reducers/getDishesReducer";
import deleteDishReducer from "./store/reducers/deleteDishReducer";
import editDishReducer from "./store/reducers/editDishReducer";
import getCurrentDishReducer from "./store/reducers/getCurrentDishReducer";
import dishOrderRender from "./store/reducers/dishOrderRender";
import createOrderReducer from "./store/reducers/createOrderReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
	addDish: dishFormReducer,
	getDishes: getDishesReducer,
	deleteDish: deleteDishReducer,
	editDish: editDishReducer,
	getDish: getCurrentDishReducer,
	addDishOrder: dishOrderRender,
	createOrder: createOrderReducer,
});

const store = createStore(rootReducer, composeEnhancers(
	applyMiddleware(thunk)
));

const app = (
	<Provider store={store}>
		<BrowserRouter>
			<App/>
		</BrowserRouter>
	</Provider>
);

ReactDOM.render(app, document.getElementById('root'));
