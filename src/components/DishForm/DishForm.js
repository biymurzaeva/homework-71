import React, {useState} from 'react';

const initialState = {
	name: '',
	price: 0,
	image: '',
};

const DishForm = ({dishData, onSubmit}) => {
	const [dishesData, setDishesData] = useState(dishData || initialState);

	const onInputChange = e => {
		const {name, value} = e.target;
		setDishesData(prev => ({
			...prev,
			[name]: value,
		}));
	};

	const addDish = async e => {
		e.preventDefault();
		onSubmit({...dishesData});
	};

	return (
		<div className="Container">
			<div className="AddDishForm">
				<form onSubmit={addDish}>
					<div className="FormRow">
						<label>Dish name: </label>
						<input
							className="Input"
							type="text"
							name="name"
							value={dishesData.name}
							onChange={onInputChange}
						/>
					</div>
					<div className="FormRow">
						<label>Price: </label>
						<input
							className="Input"
							type="text"
							name="price"
							value={dishesData.price}
							onChange={onInputChange}
						/>
					</div>
					<div className="FormRow">
						<label>Image URL: </label>
						<input
							className="Input"
							type="text"
							name="image"
							value={dishesData.image}
							onChange={onInputChange}
						/>
					</div>
					<button type="submit" className="SendBtn">Add dish</button>
				</form>
			</div>
		</div>
	);
};

export default DishForm;