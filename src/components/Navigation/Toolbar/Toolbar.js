import React from 'react';
import './Toolbar.css';
import {NavLink} from "react-router-dom";

const Toolbar = () => {
  return (
    <header className="Toolbar">
      <div className="Toolbar-title">
        Turtle Pizza Admin
      </div>
      <nav>
        <ul className="NavigationItems">
          <li className="NavigationItem">
            <NavLink to="/dishes" exact >Dishes</NavLink>
          </li>
          <li className="NavigationItem">
            <NavLink to="/orders">Orders</NavLink>
          </li>
          <li className="NavigationItem">
            <NavLink to="/createOrder">Create an order</NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Toolbar;