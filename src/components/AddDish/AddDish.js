import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {addDish as dish} from "../../store/actions/dishFormAction";
import './AddDish.css';

const AddDish = ({history}) => {
	const dispatch = useDispatch();

	const [dishInputName, setDishInputName] = useState({
		name: '',
		price: 0,
		image: '',
	});

	const onInputChange = e => {
		const {name, value} = e.target;
		setDishInputName(prev => ({
			...prev,
			[name]: value,
		}));
	};

	const addDish = async e => {
		e.preventDefault();
		await dispatch(dish(dishInputName));
		history.push('/');
	};

	return (
		<div className="Container">
			<div className="AddDishForm">
				<form onSubmit={addDish}>
					<div className="FormRow">
						<label>Dish name: </label>
						<input
							className="Input"
							type="text"
							name="name"
							value={dishInputName.name}
							onChange={onInputChange}
						/>
					</div>
					<div className="FormRow">
						<label>Price: </label>
						<input
							className="Input"
							type="text"
							name="price"
							value={dishInputName.price}
							onChange={onInputChange}
						/>
					</div>
					<div className="FormRow">
						<label>Image URL: </label>
						<input
							className="Input"
							type="text"
							name="image"
							value={dishInputName.image}
							onChange={onInputChange}
						/>
					</div>
					<button type="submit" className="SendBtn">Add dish</button>
				</form>
			</div>
		</div>
	);
};

export default AddDish;