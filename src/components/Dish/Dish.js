import React from 'react';
import './Dish.css';

const Dish = props => {
	return (
		<div className="Dish">
			<div className="DishImageName">
				<img src={props.image} alt={props.name} className="ImgDish"/>
				<h3>{props.name}</h3>
			</div>
			<div className="DishPriceButtons">
				<p><strong>{props.price} KGS</strong></p>
				<button type="button" className="EditBtn" onClick={props.editDish}>Edit</button>
				<button type="button" className="DeleteBtn" onClick={props.deleteDish}>Delete</button>
			</div>
		</div>
	);
};

export default Dish;