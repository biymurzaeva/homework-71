import React, {useEffect, useState} from 'react';
import {DISHES} from "../../contacts";

const OrderSummary = props => {
	const [totalPrice, setTotalPrice] = useState(0);

	useEffect(() => {
		const price = Object.keys(props.dishCount).reduce((acc, type) => {
			return acc + DISHES[type].price * props.dishCount[type];
		}, 150);

		setTotalPrice(price);
	}, [props.dishCount]);

	return  (
		<>
			<ul>
				{Object.keys(props.dishCount).map((type, i) => {
					if (props.dishes[i].name.toLowerCase() === type) {
						return (
							<li key={type}>
		            <span style={{textTransform: 'capitalize'}}>
			            {type}
		            </span>
								: x{props.dishCount[type]}
								<span> {props.dishes[i].price} KGS</span>
							</li>
						);
					}
					return type;
				})}
			</ul>
			<p><strong>Delivery: {props.delivery} KGS</strong></p>
			<p><strong>Total: {totalPrice} KGS</strong></p>
			<button onClick={props.onCancel}>Cancel</button>
			<button onClick={props.onOrder}>Order</button>
		</>
	);
};

export default OrderSummary;