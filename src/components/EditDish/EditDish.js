import React from 'react';
import DishForm from "../DishForm/DishForm";
import {useDispatch, useSelector} from "react-redux";
import {editDish} from "../../store/actions/editDishAction";

const EditDish = ({history, match}) => {
	const dispatch = useDispatch();
	const dish = useSelector(state => state.getDish.dish);

	const onSubmit = async dish => {
		await dispatch(editDish(match.params.id, {...dish}));
		history.push('/');
	};

	return (
		<DishForm
			dishData={dish}
			onSubmit={onSubmit}
		/>
	);
};

export default EditDish;