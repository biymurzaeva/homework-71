import React, {useEffect} from 'react';
import './Dishes.css';
import {useDispatch, useSelector} from "react-redux";
import {getDish} from "../../store/actions/getDishesAction";
import {deleteDish} from "../../store/actions/deleteDishAction";
import Dish from "../../components/Dish/Dish";
import {getCurrentDish} from "../../store/actions/getCurrentDishAction";
const Dishes = ({history}) => {
	const dispatch = useDispatch();
	const dishes = useSelector(state => state.getDishes.dishes);

	useEffect(() => {
		dispatch(getDish());
	}, [dispatch]);

	const addNewDish = () => {
		history.push('/addDish');
	};

	const removeDish = async id => {
		await dispatch(deleteDish(id));
	  dispatch(getDish());
	};

	const editDish = async id => {
		await dispatch(getCurrentDish(id));
		history.push(`/dishes/edit/${id}`);
	};

	return dishes && (
		<div className="Container">
			<div className="DishesHeader">
				<h2>Dishes</h2>
				<button className="addDishBtn" onClick={addNewDish}>Add new dish</button>
			</div>
			<div className="Dishes">
				{dishes.map(dish => (
					<Dish
						key={dish.id}
						name={dish.name}
						price={dish.price}
						image={dish.image}
						editDish={() => editDish(dish.id)}
						deleteDish={() => removeDish(dish.id)}
					/>
				))}
			</div>
		</div>
	);
};

export default Dishes;