import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getDish} from "../../store/actions/getDishesAction";
import './CreateOrder.css';
import {dishOrder, setModalOpen} from "../../store/actions/dishOrderAction";
import Modal from "../../UI/Modal/Modal";
import OrderSummary from "../../components/OrderSummary/OrderSummary";
import {createOrder as order} from "../../store/actions/createOrderAction";

const CreateOrder = ({history}) => {
	const dispatch = useDispatch();
	const dishes = useSelector(state => state.getDishes.dishes);
	const dishOrdersCount = useSelector(state => state.addDishOrder.dishCount);
	const thisModal = useSelector(state => state.addDishOrder.showPurchaseModal);
	const delivery = useSelector(state => state.addDishOrder.delivery);

	useEffect( () => {
		dispatch(getDish());
	}, [dispatch]);

	const orderCheckout = (dish, e) => {
		e.preventDefault();
		dispatch(dishOrder(dish));
	};

	const openModal = () => {
		dispatch(setModalOpen(true));
	};

	const closeModal = () => {
		dispatch(setModalOpen(false));
	};

	const createOrder = async () => {
		await dispatch(order(dishOrdersCount));
		dispatch(setModalOpen(false));
		history.push('/');
	};

	return dishes && (
		<div className="Container">
			<Modal
				show={thisModal}
				close={closeModal}
			>
				<OrderSummary
					delivery={delivery}
					dishCount={dishOrdersCount}
					dishes={dishes}
					onCancel={closeModal}
					onOrder={createOrder}
				/>
			</Modal>
			{dishes.map(dish => (
				<div key={dish.id} className="DishOrder">
					<div className="DishImageName" onClick={e => orderCheckout(dish.name.toLowerCase(), e)}>
						<img src={dish.image} alt={dish.name} className="ImgDish"/>
						<p>{dish.name} <span><strong>{dish.price}KGS</strong></span></p>
					</div>
				</div>
			))}
			<hr/>
			<div>
				{Object.keys(dishOrdersCount).map((p, i) => {
					if (dishOrdersCount[p] !== 0) {
						if (p === dishes[i].name.toLowerCase()) {
							return (
								<div className="Orders" key={dishes[i].id}>
									<p>{dishes[i].name}<span>x{dishOrdersCount[p]}</span></p>
									<span>{dishes[i].price}</span>
								</div>
							);
						}
					}
					return false;
				})}
				<button onClick={openModal}>Checkout</button>
			</div>
		</div>
	);
};

export default CreateOrder;