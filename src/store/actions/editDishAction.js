import axiosApi from "../../axiosApi";

export const EDIT_DISH_REQUEST = 'EDIT_DISH_REQUEST';
export const EDIT_DISH_SUCCESS = 'EDIT_DISH_SUCCESS';
export const EDIT_DISH_FAILURE = 'EDIT_DISH_FAILURE';

export const editDishRequest = () => ({type: EDIT_DISH_REQUEST});
export const editDishSuccess = () => ({type: EDIT_DISH_SUCCESS});
export const editDishFailure = error => ({type: EDIT_DISH_FAILURE, payload: error});

export const editDish = (dishID, dishData) => {
	return async (dispatch) => {
		try {
			// console.log(dishID, dishData);

			dispatch(editDishRequest());
			await axiosApi.put(`/dishes/${dishID}.json`, dishData);
			dispatch(editDishSuccess());
		} catch (error) {
			dispatch(editDishFailure(error));
			throw error;
		}
	}
};