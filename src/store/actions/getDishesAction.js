import axiosApi from "../../axiosApi";

export const GET_DISH_REQUEST = 'GET_DISH_REQUEST';
export const GET_DISH_SUCCESS = 'GET_DISH_SUCCESS';
export const GET_DISH_FAILURE = 'GET_DISH_FAILURE';

export const getDishRequest = () => ({type: GET_DISH_REQUEST});
export const getDishSuccess = dishes => ({type: GET_DISH_SUCCESS, payload: dishes});
export const getDishFailure = error => ({type: GET_DISH_FAILURE, payload: error});

export const getDish = () => {
	return async (dispatch) => {
		try {
			dispatch(getDishRequest());
			const response = await axiosApi.get('/dishes.json');
			const dishes = Object.keys(response.data).map(id => ({
				...response.data[id],
				id
			}));
			dispatch(getDishSuccess(dishes));
		} catch (error) {
			dispatch(getDishFailure(error));
		}
	}
};