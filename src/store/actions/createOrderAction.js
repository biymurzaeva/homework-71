import axiosApi from "../../axiosApi";

export const CREATE_ORDER_REQUEST = 'CREATE_ORDER_REQUEST';
export const CREATE_ORDER_SUCCESS = 'CREATE_ORDER_SUCCESS';
export const CREATE_ORDER_FAILURE = 'CREATE_ORDER_FAILURE';

export const createOrderRequest = () => ({type: CREATE_ORDER_REQUEST});
export const createOrderSuccess = () => ({type: CREATE_ORDER_SUCCESS});
export const createOrderFailure = error => ({type: CREATE_ORDER_FAILURE, payload: error});

export const createOrder = dishData => {
	return async (dispatch) => {
		try {
			dispatch(createOrderRequest());
			await axiosApi.post('/orders.json', dishData);
			dispatch(createOrderSuccess());
		} catch (error) {
			dispatch(createOrderFailure(error));
		}
	}
};