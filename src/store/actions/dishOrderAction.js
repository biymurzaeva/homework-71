export const DISH_ORDER = 'DISH_ORDER';
export const dishOrder = dishType => ({type: DISH_ORDER, payload: dishType});

export const SET_MODAL_OPEN = 'SET_MODAL_OPEN'
export const setModalOpen = isOpen => ({type: SET_MODAL_OPEN, payload: isOpen});