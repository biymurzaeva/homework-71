import axiosApi from "../../axiosApi";

export const GET_CURRENT_DISH_REQUEST = 'GET_CURRENT_DISH_REQUEST';
export const GET_CURRENT_DISH_SUCCESS = 'GET_CURRENT_DISH_SUCCESS';
export const GET_CURRENT_DISH_FAILURE = 'GET_DISH_FAILURE';

export const getCurrentDishRequest = () => ({type: GET_CURRENT_DISH_REQUEST});
export const getCurrentDishSuccess = dish => ({type: GET_CURRENT_DISH_SUCCESS, payload: dish});
export const getCurrentDishFailure = error => ({type: GET_CURRENT_DISH_FAILURE, payload: error});

export const getCurrentDish = dishID => {
	return async (dispatch) => {
		try {
			dispatch(getCurrentDishRequest());
			const response = await axiosApi.get(`/dishes/${dishID}.json`);
			dispatch(getCurrentDishSuccess(response.data));
		} catch (error) {
			dispatch(getCurrentDishFailure(error));
		}
	}
};

