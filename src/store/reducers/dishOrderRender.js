import {DISH_ORDER, SET_MODAL_OPEN} from "../actions/dishOrderAction";

const initialState = {
	dishCount: {
		pepperoni: 0,
		margarita: 0
	},
	showPurchaseModal: false,
	delivery: 150,
};

const dishOrderRender = (state= initialState, action) => {
	switch (action.type) {
		case DISH_ORDER:
		return {
			...state,
			dishCount: {
				...state.dishCount,
				[action.payload]: state.dishCount[action.payload] + 1
			},
		};
		case SET_MODAL_OPEN:
			return {
				...state, showPurchaseModal: action.payload
			};
		default:
			return state;
	}
}

export default dishOrderRender;