import {ADD_DISH_FAILURE, ADD_DISH_REQUEST, ADD_DISH_SUCCESS} from "../actions/dishFormAction";

const initialState = {
	loading: false,
	error: null
};

const dishFormReducer = (state = initialState, action) => {
	switch (action.payload) {
		case ADD_DISH_REQUEST:
			return {...state, loading: true}
		case ADD_DISH_SUCCESS:
			return {...state, loading: false}
		case ADD_DISH_FAILURE:
			return {...state, error: action.payload}
		default:
			return state;
	}
};

export default dishFormReducer;
