import {DELETE_DISH_FAILURE, DELETE_DISH_REQUEST, DELETE_DISH_SUCCESS} from "../actions/deleteDishAction";

export const initialState = {
	loading: false,
	error: null
};

export const deleteDishReducer = (state = initialState, action) => {
	switch (action.type) {
		case DELETE_DISH_REQUEST:
			return {...state, loading: true}
		case DELETE_DISH_SUCCESS:
			return {...state, loading: false}
		case DELETE_DISH_FAILURE:
			return {...state, loading: false, error: action.payload}
		default:
			return state;
	}
};

export default deleteDishReducer;