import {CREATE_ORDER_FAILURE, CREATE_ORDER_REQUEST, CREATE_ORDER_SUCCESS} from "../actions/createOrderAction";

const initialState = {
	loading: false,
	error: null
};

const createOrderReducer = (state = initialState, action) => {
	switch (action.payload) {
		case CREATE_ORDER_REQUEST:
			return {...state, loading: true}
		case CREATE_ORDER_SUCCESS:
			return {...state, loading: false}
		case CREATE_ORDER_FAILURE:
			return {...state, error: action.payload}
		default:
			return state;
	}
};

export default createOrderReducer;