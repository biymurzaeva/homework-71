import {GET_DISH_FAILURE, GET_DISH_REQUEST, GET_DISH_SUCCESS} from "../actions/getDishesAction";

const initialState = {
	dishes: [],
	error: null,
	loading: false,
};

const getDishesReducer = (state = initialState, action) => {
	switch (action.type) {
		case GET_DISH_REQUEST:
			return {...state, loading: true}
		case GET_DISH_SUCCESS:
			return {...state, loading: false, dishes: action.payload}
		case GET_DISH_FAILURE:
			return {...state, error: action.payload}
		default:
			return state;
	}
};

export default getDishesReducer;