import {EDIT_DISH_FAILURE, EDIT_DISH_REQUEST, EDIT_DISH_SUCCESS} from "../actions/editDishAction";

const initialState = {
	loading: false,
	error: null
};

const editDishReducer = (state = initialState, action) => {
	switch (action.type) {
		case EDIT_DISH_REQUEST:
			return {...state, loading: true}
		case EDIT_DISH_SUCCESS:
			return {...state, loading: false}
		case EDIT_DISH_FAILURE:
			return {...state, loading: false, error: action.payload}
		default:
			return state;
	}
};

export default editDishReducer;