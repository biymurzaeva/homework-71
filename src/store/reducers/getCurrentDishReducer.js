import {GET_CURRENT_DISH_FAILURE, GET_CURRENT_DISH_REQUEST, GET_CURRENT_DISH_SUCCESS} from "../actions/getCurrentDishAction";

const initialState = {
	dish: {},
	error: null,
	loading: false,
};

const getCurrentDishReducer = (state = initialState, action) => {
	switch (action.type) {
		case GET_CURRENT_DISH_REQUEST:
			return {...state, loading: true}
		case GET_CURRENT_DISH_SUCCESS:
			return {...state, loading: false, dish: action.payload}
		case GET_CURRENT_DISH_FAILURE:
			return {...state, error: action.payload}
		default:
			return state;
	}
};

export default getCurrentDishReducer;