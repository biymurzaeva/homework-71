import './App.css';
import {Route, Switch} from "react-router-dom";
import Layout from "./UI/Layout/Layout";
import Dishes from "./containers/Dishes/Dishes";
import Orders from "./containers/Orders/Orders";
import AddDish from "./components/AddDish/AddDish";
import EditDish from "./components/EditDish/EditDish";
import CreateOrder from "./containers/CreateOrder/CreateOrder";

const App = () => (
  <Layout>
    <Switch>
      <Route path="/" exact component={Dishes}/>
      <Route path="/dishes/edit/:id" component={EditDish}/>
      <Route path="/dishes" component={Dishes}/>
      <Route path="/orders" component={Orders}/>
      <Route path="/createOrder" component={CreateOrder}/>
      <Route path="/addDish" component={AddDish}/>
      <Route render={() => <h1>Not found</h1>}/>
    </Switch>
  </Layout>
);

export default App;
